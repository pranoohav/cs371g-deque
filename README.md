# CS371g: Generic Programming Deque Repo

* Name: Pranooha Veeramachaneni and Viswanath Kasireddy

* EID: pv5749 and vrk352

* GitLab ID: viswa_kasireddy

* Git SHA: (most recent Git SHA, final change to your repo will change this, that's ok) e3725ff76c50da77fe8ba8d6bbb0e6dbefba8902

* GitLab Pipelines: https://gitlab.com/pranoohav/cs371g-deque/-/pipelines

* Estimated completion time: 20

* Actual completion time: (actual time in hours, int or float) 20

* Comments: (any additional comments you have)
