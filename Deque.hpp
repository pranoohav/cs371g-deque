// -------
// Deque.h
// --------

#ifndef Deque_h
#define Deque_h

// --------
// includes
// --------

#include <algorithm>        // copy, equal, lexicographical_compare, max, swap
#include <cassert>          // assert
#include <initializer_list> // initializer_list
#include <iterator>         // bidirectional_iterator_tag
#include <memory>           // allocator
#include <stdexcept>        // out_of_range
#include <utility>          // !=, <=, >, >=

// -----
// using
// -----

using std::rel_ops::operator!=;
using std::rel_ops::operator<=;
using std::rel_ops::operator>;
using std::rel_ops::operator>=;

// -------
// destroy
// -------

template <typename A, typename BI>
BI my_destroy (A& a, BI b, BI e) {
    while (b != e) {
        --e;
        a.destroy(&*e);
    }
    return b;
}

// ------------------
// uninitialized_copy
// ------------------

template <typename A, typename II, typename BI>
BI my_uninitialized_copy (A& a, II b, II e, BI x) {
    BI p = x;
    try {
        while (b != e) {
            a.construct(&*x, *b);
            ++b;
            ++x;
        }
    }
    catch (...) {
        my_destroy(a, p, x);
        throw;
    }
    return x;
}

// ------------------
// uninitialized_fill
// ------------------

template <typename A, typename BI, typename T>
void my_uninitialized_fill (A& a, BI b, BI e, const T& v) {
    BI p = b;
    try {
        while (b != e) {
            a.construct(&*b, v);
            ++b;
        }
    }
    catch (...) {
        my_destroy(a, p, b);
        throw;
    }
}

// --------
// my_deque
// --------

template <typename T, typename A = std::allocator<T>>
class my_deque {
    // -----------
    // operator ==
    // -----------

    /**
     * your documentation
     */
    friend bool operator == (const my_deque& lhs, const my_deque& rhs) {
        // <your code>
        // you must use std::equal()
        return std::equal(lhs.begin(), lhs.end(), rhs.begin(), rhs.end());
    }

    // ----------
    // operator <
    // ----------

    /**
     * your documentation
     */
    friend bool operator < (const my_deque& lhs, const my_deque& rhs) {
        // <your code>
        // you must use std::lexicographical_compare()
        return std::lexicographical_compare(lhs.begin(), lhs.end(), rhs.begin(), rhs.end());
    }

    // ----
    // swap
    // ----

    /**
     * your documentation
     */
    friend void swap (my_deque& x, my_deque& y) {
        x.swap(y);
    }

public:
    // ------
    // usings
    // ------

    // you must use this allocator for the inner arrays
    using allocator_type  = A;
    using value_type      = typename allocator_type::value_type;

    using size_type       = typename allocator_type::size_type;
    using difference_type = typename allocator_type::difference_type;

    using pointer         = typename allocator_type::pointer;
    using const_pointer   = typename allocator_type::const_pointer;

    using reference       = typename allocator_type::reference;
    using const_reference = typename allocator_type::const_reference;

    // you must use this allocator for the outer array
    using allocator_type_2 = typename A::template rebind<pointer>::other;

private:
    // ----
    // data
    // ----
    pointer* outer_start;   //outer start pointers
    pointer* outer_end;   //outer end pointer
    size_type size_used;  //how many elements in the deque
    size_type size_capacity;  //how much total space for elements there is in outer array
    size_type in_use_start;   //where the data starts
    size_type in_use_end;    //where the data ends
    size_type num_inner;    //number of inner arrays deque has

    const size_type static inner_size = 10;

    allocator_type _a;  //creates space in inner
    allocator_type_2 _outer; //creates space in outer


    // <your data>

private:
    // -----
    // valid
    // -----

    bool valid () const {
        // <your code>
        return true;
    }

public:
    // --------
    // iterator
    // --------

    class iterator {
        // -----------
        // operator ==
        // -----------

        /**
         * your documentation
         */
        friend bool operator == (const iterator& lhs, const iterator& rhs) {
            // <your code>

            return (lhs.index == rhs.index) && (lhs.input == rhs.input);
        }

        /**
         * your documentation
         */
        friend bool operator != (const iterator& lhs, const iterator& rhs) {
            return !(lhs == rhs);
        }

        // ----------
        // operator +
        // ----------

        /**
         * your documentation
         */
        friend iterator operator + (iterator lhs, difference_type rhs) {
            return lhs += rhs;
        }

        // ----------
        // operator -
        // ----------

        /**
         * your documentation
         */
        friend iterator operator - (iterator lhs, difference_type rhs) {
            return lhs -= rhs;
        }

    public:
        // ------
        // usings
        // ------

        // this requires a weaker iterator than the real deque provides
        using iterator_category = std::bidirectional_iterator_tag;
        using value_type        = typename my_deque::value_type;
        using difference_type   = typename my_deque::difference_type;
        using pointer           = typename my_deque::pointer;
        using reference         = typename my_deque::reference;

    private:
        // ----
        // data
        // ----
        my_deque<T,A>* input;
        size_type index;

        // <your data>

    private:
        // -----
        // valid
        // -----

        bool valid () const {
            // <your code>
            return true;
        }

    public:
        // -----------
        // constructor
        // -----------

        /**
         * your documentation
         */
        iterator (my_deque<T,A>* input_deque) {
            input = input_deque;
            index = 0;
            assert(valid());
        }

        iterator (my_deque<T,A>* input_deque, size_type i) {
            // <your code>
            input = input_deque;
            index = i;
            assert(valid());
        }

        iterator             ()                = default;
        iterator             (const iterator&) = default;
        ~iterator            ()                = default;
        iterator& operator = (const iterator&) = default;

        // ----------
        // operator *
        // ----------

        /**
         * your documentation
         */
        reference operator * () const {
            // <your code>
            return (*input)[index];
        }

        // -----------
        // operator ->
        // -----------

        /**
         * your documentation
         */
        pointer operator -> () const {
            return &(*input)[index];
        }

        // -----------
        // operator ++
        // -----------

        /**
         * your documentation
         */
        iterator& operator ++ () {
            // <your code>
            ++index;
            assert(valid());
            return *this;
        }

        /**
         * your documentation
         */
        iterator operator ++ (int) {
            iterator x = *this;
            ++(*this);
            assert(valid());
            return x;
        }

        // -----------
        // operator --
        // -----------

        /**
         * your documentation
         */
        iterator& operator -- () {
            // <your code>
            --index;
            assert(valid());
            return *this;
        }

        /**
         * your documentation
         */
        iterator operator -- (int) {
            iterator x = *this;
            --(*this);
            assert(valid());
            return x;
        }

        // -----------
        // operator +=
        // -----------

        /**
         * your documentation
         */
        iterator& operator += (difference_type d) {
            // <your code>
            index += d;
            assert(valid());
            return *this;
        }

        // -----------
        // operator -=
        // -----------

        /**
         * your documentation
         */
        iterator& operator -= (difference_type d) {
            // <your code>
            index -= d;
            assert(valid());
            return *this;
        }
    };

public:
    // --------------
    // const_iterator
    // --------------

    class const_iterator {
        // -----------
        // operator ==
        // -----------

        /**
         * your documentation
         */
        friend bool operator == (const const_iterator& lhs, const const_iterator& rhs) {
            // <your code>
            return lhs.index == rhs.index && (lhs.input == rhs.input);
        }

        /**
         * your documentation
         */
        friend bool operator != (const const_iterator& lhs, const const_iterator& rhs) {
            return !(lhs == rhs);
        }

        // ----------
        // operator +
        // ----------

        /**
         * your documentation
         */
        friend const_iterator operator + (const_iterator lhs, difference_type rhs) {
            return lhs += rhs;
        }

        // ----------
        // operator -
        // ----------

        /**
         * your documentation
         */
        friend const_iterator operator - (const_iterator lhs, difference_type rhs) {
            return lhs -= rhs;
        }

    public:
        // ------
        // usings
        // ------

        // this requires a weaker iterator than the real deque provides
        using iterator_category = std::bidirectional_iterator_tag;
        using value_type        = typename my_deque::value_type;
        using difference_type   = typename my_deque::difference_type;
        using pointer           = typename my_deque::const_pointer;
        using reference         = typename my_deque::const_reference;

    private:
        // ----
        // data
        // ----

        // <your data>
        const my_deque<T,A>* input;
        size_type index;


    private:
        // -----
        // valid
        // -----

        bool valid () const {
            // <your code>
            return true;
        }

    public:
        // -----------
        // constructor
        // -----------

        /**
         * your documentation
         */

        const_iterator (const my_deque<T,A>* input_deque) {
            input = input_deque;
            index = 0;
            assert(valid());
        }

        const_iterator (const my_deque<T,A>* input_deque, size_type i) {
            input = input_deque;
            index = i;
            assert(valid());
        }

        const_iterator             ()                      = default;
        const_iterator             (const const_iterator&) = default;
        ~const_iterator            ()                      = default;
        const_iterator& operator = (const const_iterator&) = default;

        // ----------
        // operator *
        // ----------

        /**
         * your documentation
         */
        reference operator * () const {
            // <your code>
            return (*input)[index];
        }

        // -----------
        // operator ->
        // -----------

        /**
         * your documentation
         */
        pointer operator -> () const {
            return &(*input)[index];
        }

        // -----------
        // operator ++
        // -----------

        /**
         * your documentation
         */
        const_iterator& operator ++ () {
            // <your code>
            ++index;
            assert(valid());
            return *this;
        }

        /**
         * your documentation
         */
        const_iterator operator ++ (int) {
            const_iterator x = *this;
            ++(*this);
            assert(valid());
            return x;
        }

        // -----------
        // operator --
        // -----------

        /**
         * your documentation
         */
        const_iterator& operator -- () {
            // <your code>
            --index;
            assert(valid());
            return *this;
        }

        /**
         * your documentation
         */
        const_iterator operator -- (int) {
            const_iterator x = *this;
            --(*this);
            assert(valid());
            return x;
        }

        // -----------
        // operator +=
        // -----------

        /**
         * your documentation
         */
        const_iterator& operator += (difference_type n) {
            // <your code>
            index += n;
            assert(valid());
            return *this;
        }

        // -----------
        // operator -=
        // -----------

        /**
         * your documentation
         */
        const_iterator& operator -= (difference_type n) {
            // <your code>
            index -= n;
            assert(valid());
            return *this;
        }
    };

public:
    // ------------
    // constructors
    // ------------

    my_deque () {
        size_used = 0;
        outer_start = _outer.allocate(1);
        *outer_start = _a.allocate(inner_size);
        outer_end = outer_start + 1;
        in_use_start = 1;
        in_use_end = in_use_start;
        num_inner = 1;
        size_capacity = num_inner * inner_size;
        assert(valid());
    }

    /**
     * your documentation
     */
    explicit my_deque (size_type s) {
        // <your code>
        size_used = s;
        num_inner = (s/inner_size) + 2;
        outer_start = _outer.allocate(num_inner);
        outer_end = outer_start + num_inner;
        size_capacity = num_inner * inner_size;
        for(int i = 0; i < num_inner; i++) {
            outer_start[i] = _a.allocate(inner_size);
        }
        in_use_start = size_capacity/3;
        in_use_end = in_use_start + s;
        my_uninitialized_fill(_a, begin(), end(), value_type());
        assert(valid());
    }

    /**
     * your documentation
     */
    my_deque (size_type s, const_reference v) {
        // <your code>
        size_used = s;
        num_inner = (s/inner_size) + 2;
        outer_start = _outer.allocate(num_inner);
        outer_end = outer_start + num_inner;
        size_capacity = num_inner * inner_size;
        for(int i = 0; i < num_inner; i++) {
            outer_start[i] = _a.allocate(inner_size);
        }
        in_use_start = size_capacity/3;
        in_use_end = in_use_start + s;
        my_uninitialized_fill(_a, begin(), end(), v);
        assert(valid());
    }

    /**
     * your documentation
     */
    my_deque (size_type s, const_reference v, const allocator_type& a) {
        // <your code>
        size_used = s;
        num_inner = (s/inner_size) + 2;
        outer_start = _outer.allocate(num_inner);
        outer_end = outer_start + num_inner;
        size_capacity = num_inner * inner_size;
        for(int i = 0; i < num_inner; i++) {
            outer_start[i] = a.allocate(inner_size);
        }
        in_use_start = size_capacity/3;
        in_use_end = in_use_start + s;
        my_uninitialized_fill(a, begin(), end(), v);
        assert(valid());
    }

    /**
     * your documentation
     */
    my_deque (std::initializer_list<value_type> rhs): _a() {
        if (rhs.size() != 0) {
            size_used = rhs.size_used;
            num_inner = rhs.num_inner;
            outer_start = _outer.allocate(num_inner);
            for(int i = 0; i < num_inner; i++) {
                outer_start[i] = _a.allocate(inner_size);
            }
            outer_end = rhs.outer_end;
            in_use_start = rhs.in_use_start;
            in_use_end = rhs.in_use_end;
            size_capacity = rhs.size_capacity;
            my_uninitialized_copy(_a, rhs.begin(), rhs.end(), begin());
        }
        assert(valid());
    }


    /**
     * your documentation
     */
    my_deque (std::initializer_list<value_type> rhs, const allocator_type& a): _a(a) {
        if (rhs.size() != 0) {
            num_inner = rhs.num_inner;
            outer_start = _outer.allocate(num_inner);
            for(int i = 0; i < num_inner; i++) {
                outer_start[i] = _a.allocate(inner_size);
            }
            outer_end = rhs.outer_end;
            in_use_start = rhs.in_use_start;
            in_use_end = rhs.in_use_end;
            size_used = rhs.size_used;
            size_capacity = rhs.size_capacity;
            my_uninitialized_copy(_a, rhs.begin(), rhs.end(), begin());
        }
        assert(valid());
    }

    /**
     * your documentation
     */
    my_deque (const my_deque& that) {
        // <your code>
        num_inner = that.num_inner;
        outer_start = _outer.allocate(num_inner);
        for(int i = 0; i < num_inner; i++) {
            outer_start[i] = _a.allocate(inner_size);
        }
        outer_end = that.outer_end;
        in_use_start = that.in_use_start;
        in_use_end = that.in_use_end;
        size_used = that.size_used;
        size_capacity = that.size_capacity;
        my_uninitialized_copy(_a, that.begin(), that.end(), begin());
        assert(valid());
    }

    // ----------
    // destructor
    // ----------

    /**
     * your documentation
     */
    ~my_deque () {
        // <your code>
        my_destroy(_a, begin(), end());
        for(int i = 0; i < num_inner; i++) {
            _a.deallocate(outer_start[i], inner_size);
        }
        _outer.deallocate(outer_start, num_inner);
        assert(valid());
    }

    // ----------
    // operator =
    // ----------

    /**
     * your documentation
     */
    my_deque& operator = (const my_deque& rhs) {
        // <your code>
        my_deque tmp(rhs);
        *this = tmp;
        assert(valid());
        return *this;
    }

    // -----------
    // operator []
    // -----------

    /**
     * your documentation
     */
    reference operator [] (size_type index) {
        // <your code>
        size_type actual = index + in_use_start;
        size_type inner = actual/inner_size;
        return outer_start[inner][actual % inner_size];
    }

    /**
     * your documentation
     */
    const_reference operator [] (size_type index) const {
        return const_cast<my_deque*>(this)->operator[](index);
    }

    // --
    // at
    // --

    /**
     * your documentation
     * @throws out_of_range
     */
    reference at (size_type index) {
        // <your code>
        if(index < 0 || index >=size()) {
            throw std::out_of_range("Index is not in the deque range");
        }
        return (*this)[index];
    }

    /**
     * your documentation
     * @throws out_of_range
     */
    const_reference at (size_type index) const {
        return const_cast<my_deque*>(this)->at(index);
    }

    // ----
    // back
    // ----

    /**
     * your documentation
     */
    reference back () {
        // <your code>
        return at(size() - 1);
    }

    /**
     * your documentation
     */
    const_reference back () const {
        return const_cast<my_deque*>(this)->back();
    }

    // -----
    // begin
    // -----

    /**
     * your documentation
     */
    iterator begin () {
        // <your code>
        return iterator(this,0);
    }

    /**
     * your documentation
     */
    const_iterator begin () const {
        // <your code>
        return const_iterator(this,0);
    }

    // -----
    // clear
    // -----

    /**
     * your documentation
     */
    void clear () {
        // <your code>
        *this = my_deque();
        assert(valid());
    }

    // -----
    // empty
    // -----

    /**
     * your documentation
     */
    bool empty () const {
        return !size();
    }

    // ---
    // end
    // ---

    /**
     * your documentation
     */
    iterator end () {
        // <your code>
        return iterator(this, size());
    }

    /**
     * your documentation
     */
    const_iterator end () const {
        // <your code>
        return const_iterator(this, size());
    }

    // -----
    // erase
    // -----

    /**
     * your documentation
     */
    iterator erase (iterator t) {
        // <your code>
        if(t == begin()) {
            pop_front();
        } else if (t == end()) {
            pop_back();
        } else {
            iterator tmp = begin();
            while(tmp != t) {
                ++tmp;
            }
            while(tmp + 1 != end()) {
                *tmp = *(tmp + 1);
                ++tmp;
            }
            _a.destroy(&*(--end()));
            --in_use_end;
        }
        assert(valid());
        return begin();
    }

    // -----
    // front
    // -----

    /**
     * your documentation
     */
    reference front () {
        // <your code>
        return (*this)[0];
    }

    /**
     * your documentation
     */
    const_reference front () const {
        return const_cast<my_deque*>(this)->front();
    }

    // ------
    // insert
    // ------

    /**
     * your documentation
     */
    iterator insert (iterator t, const_reference val) {
        // <your code>
        iterator tmp;
        bool top = false;
        if(std::distance(begin(),t) <= std::distance(t, end())) {
            top = true;
        }
        if(in_use_start == 0 || (in_use_end == size_capacity)) {
            my_deque temp(2*size_capacity);
            my_uninitialized_copy(_outer, begin(), end(), temp.begin());
            *this = temp;
        }
        if(top) {
            tmp = begin();
            --in_use_start;
            while(tmp != t) {
                *tmp = *(tmp + 1);
                ++tmp;
            }
            _a.construct(&*tmp, val);
        } else {
            tmp = end();
            ++in_use_end;
            while(tmp != t) {
                *tmp = *(tmp - 1);
                --tmp;
            }
            _a.construct(&*tmp, val);
        }
        ++size_used;
        assert(valid());
        return tmp;
    }

    // ---
    // pop
    // ---

    /**
     * your documentation
     */
    void pop_back () {
        // <your code>
        // erase(--end());
        _a.destroy(&*(--end()));
        --in_use_end;
        assert(valid());
    }

    /**
     * your documentation
     */
    void pop_front () {
        // <your code>
        // erase(begin());
        _a.destroy(&*begin());
        ++in_use_start;
        assert(valid());
    }

    // ----
    // push
    // ----

    /**
     * your documentation
     */
    void push_back (const_reference elem) {
        // <your code>
        insert(end(), elem);
        assert(valid());
    }

    /**
     * your documentation
     */
    void push_front (const_reference elem) {
        // <your code>
        insert(begin(), elem);
        assert(valid());
    }

    // ------
    // resize
    // ------

    /**
     * your documentation
     */
    void resize (size_type s) {
        // <your code>
        resize(s, value_type());
        assert(valid());
    }

    /**
     * your documentation
     */
    void resize (size_type s, const_reference v) {
        // <your code>
        if(s != size_used) {
            if(s < size_used) {
                my_destroy(_a,begin() + s, end());
                in_use_end = in_use_start + s;
            }
            else {
                if(in_use_start + s > size_capacity) {
                    my_deque temp(2*size_capacity);
                    my_uninitialized_copy(_outer, begin(), end(), temp.begin());
                    *this = temp;
                }
                my_uninitialized_fill(_a, end(), end() + (s - size_used), v);
                in_use_end += s - size_used;
            }
        }
        assert(valid());
    }

    // ----
    // size
    // ----

    /**
     * your documentation
     */
    size_type size () const {
        // <your code>
        return in_use_end - in_use_start;
    }


    // ----
    // swap
    // ----

    /**
     * your documentation
     */
    void swap (my_deque& other) {
        // <your code>
        my_deque tmp = *this;
        *this = other;
        other = tmp;
        assert(valid());
    }
};

#endif // Deque_h
