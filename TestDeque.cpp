// -------------
// TestDeque.c++
// -------------

// http://www.cplusplus.com/reference/deque/deque/

// --------
// includes
// --------

#include <deque>     // deque
#include <stdexcept> // invalid_argument, out_of_range

#include "gtest/gtest.h"

#include "Deque.hpp"

// -----
// Using
// -----

using namespace std;
using namespace testing;

// -----
// Types
// -----

template <typename T>
struct DequeFixture : Test {
    using deque_type    = T;
    using allocator_type = typename deque_type::allocator_type;
    using iterator       = typename deque_type::iterator;
    using const_iterator = typename deque_type::const_iterator;
};

using
deque_types =
    Types<
    deque<int>,
    my_deque<int>,
    deque<int, allocator<int>>,
    my_deque<int, allocator<int>>>;

#ifdef __APPLE__
TYPED_TEST_CASE(DequeFixture, deque_types,);
#else
TYPED_TEST_CASE(DequeFixture, deque_types);
#endif

// -----
// Tests
// -----

TYPED_TEST(DequeFixture, test1) {
    using deque_type = typename TestFixture::deque_type;
    const deque_type x;
    ASSERT_TRUE(x.empty());
    ASSERT_EQ(x.size(), 0u);
}

TYPED_TEST(DequeFixture, test2) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x;
    iterator b = begin(x);
    iterator e = end(x);
    EXPECT_TRUE(b == e);
}

TYPED_TEST(DequeFixture, test3) {
    using deque_type     = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;
    const deque_type x;
    const_iterator b = begin(x);
    const_iterator e = end(x);
    EXPECT_TRUE(b == e);
}

TYPED_TEST(DequeFixture, test4) {
    using deque_type = typename TestFixture::deque_type;
    // using iterator   = typename TestFixture::iterator;
    deque_type x;
    deque_type y;
    EXPECT_TRUE((x == y));
}

TYPED_TEST(DequeFixture, test8) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(4, 2);
    iterator b = x.begin();
    iterator e = (b+1);
    iterator c = (++b);
    EXPECT_TRUE(c == e);
}

TYPED_TEST(DequeFixture, test9) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(4, 2);
    iterator b = x.begin();
    iterator e = (b+2);
    iterator c = ++++b;
    EXPECT_TRUE(c == e);
}

TYPED_TEST(DequeFixture, test10) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(4, 2);
    iterator b = x.end();
    iterator e = (b-1);
    iterator c = --b;
    EXPECT_TRUE(c == e);
}

TYPED_TEST(DequeFixture, test11) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(4, 2);
    iterator b = x.end();
    iterator e = (b-2);
    iterator c = --(--b);
    EXPECT_TRUE(c == e);
}

TYPED_TEST(DequeFixture, test12) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(4, 2);
    iterator b = x.begin();
    x.push_back(2);
    iterator c = b+1;
    b+=1;
    EXPECT_TRUE(b == c);
}

TYPED_TEST(DequeFixture, test13) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(4, 2);
    x.push_front(2);
    iterator b = x.end();
    iterator c = b-1;
    b-=1;
    EXPECT_TRUE(b == c);
}

TYPED_TEST(DequeFixture, test14) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(2, 1);
    iterator b = x.begin();
    EXPECT_TRUE((*b == 1));
}

TYPED_TEST(DequeFixture, test15) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(2, 1);
    iterator b = x.begin();
    EXPECT_TRUE((*(++b) == 1));
}

TYPED_TEST(DequeFixture, test16) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(2, 1);
    iterator b = x.end();
    EXPECT_TRUE((*(b-1) == 1));
}

TYPED_TEST(DequeFixture, test17) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(2, 1);
    iterator b = x.begin();
    EXPECT_TRUE((*b == 1));
}

TYPED_TEST(DequeFixture, test18) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(2, 1);
    iterator b = x.begin();
    *b = 2;
    EXPECT_TRUE((*b == 2));
}

TYPED_TEST(DequeFixture, test19) {
    using deque_type = typename TestFixture::deque_type;
    using const_iterator   = typename TestFixture::const_iterator;
    const deque_type x(2,1);
    const_iterator b = x.begin();
    const_iterator c = x.begin();
    EXPECT_TRUE(b==c);
}

TYPED_TEST(DequeFixture, test20) {
    using deque_type = typename TestFixture::deque_type;
    using const_iterator   = typename TestFixture::const_iterator;
    const deque_type x(2,1);
    const_iterator b = x.begin();
    const_iterator e = b+1;
    const_iterator c = ++b;
    EXPECT_TRUE(c==e);
}

TYPED_TEST(DequeFixture, test21) {
    using deque_type = typename TestFixture::deque_type;
    using const_iterator   = typename TestFixture::const_iterator;
    const deque_type x(2,1);
    const_iterator b = x.end();
    const_iterator e = b-1;
    const_iterator c = --b;
    EXPECT_TRUE(c==e);
}

TYPED_TEST(DequeFixture, test22) {
    using deque_type = typename TestFixture::deque_type;
    using const_iterator   = typename TestFixture::const_iterator;
    const deque_type x(2,1);
    const_iterator b = x.begin();
    EXPECT_TRUE(*b == 1);
}

TYPED_TEST(DequeFixture, test25) {
    using deque_type = typename TestFixture::deque_type;
    // using const_iterator   = typename TestFixture::const_iterator;
    deque_type x(4,2);
    int a = x[3];
    ASSERT_EQ(a,2);
}

TYPED_TEST(DequeFixture, test26) {
    using deque_type = typename TestFixture::deque_type;
    // using const_iterator   = typename TestFixture::const_iterator;
    const deque_type x(4,2);
    int a = x[3];
    ASSERT_EQ(a,2);
}

TYPED_TEST(DequeFixture, test27) {
    using deque_type = typename TestFixture::deque_type;
    // using const_iterator   = typename TestFixture::const_iterator;
    deque_type x(4,2);
    int a = x.at(3);
    ASSERT_EQ(a,2);
}

TYPED_TEST(DequeFixture, test28) {
    using deque_type = typename TestFixture::deque_type;
    // using const_iterator   = typename TestFixture::const_iterator;
    const deque_type x(4,2);
    int a = x.at(3);
    ASSERT_EQ(a,2);
}

TYPED_TEST(DequeFixture, test29) {
    using deque_type = typename TestFixture::deque_type;
    // using iterator   = typename TestFixture::iterator;
    deque_type x(4,2);
    int a = x.back();
    ASSERT_EQ(a,2);
}

TYPED_TEST(DequeFixture, test30) {
    using deque_type = typename TestFixture::deque_type;
    // using iterator   = typename TestFixture::iterator;
    deque_type x(4,2);
    int a = x.front();
    ASSERT_EQ(a,2);
}

TYPED_TEST(DequeFixture, test31) {
    using deque_type = typename TestFixture::deque_type;
    // using iterator   = typename TestFixture::iterator;
    deque_type x(4,2);
    x.erase(x.begin());
    ASSERT_EQ(x.size(),3);
}

TYPED_TEST(DequeFixture, test32) {
    using deque_type = typename TestFixture::deque_type;
    // using iterator   = typename TestFixture::iterator;
    deque_type x(4,2);
    x.insert(x.begin(), 2);
    ASSERT_EQ(x.size(),5);
}

TYPED_TEST(DequeFixture, test33) {
    using deque_type = typename TestFixture::deque_type;
    // using iterator   = typename TestFixture::iterator;
    deque_type x(4,2);
    x.pop_back();
    ASSERT_EQ(x.size(),3);
}

TYPED_TEST(DequeFixture, test34) {
    using deque_type = typename TestFixture::deque_type;
    // using iterator   = typename TestFixture::iterator;
    deque_type x(4,2);
    x.pop_front();
    ASSERT_EQ(x.size(),3);
}

TYPED_TEST(DequeFixture, test35) {
    using deque_type = typename TestFixture::deque_type;
    // using iterator   = typename TestFixture::iterator;
    deque_type x(4,2);
    x.push_back(1);
    int a = x.back();
    ASSERT_EQ(a,1);
    ASSERT_EQ(x.size(),5);
}

TYPED_TEST(DequeFixture, test36) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(4,2);
    x.push_front(1);
    int a = x.front();
    iterator b = x.begin();
    ASSERT_EQ(a,1);
    ASSERT_EQ(x.size(),5);
}

TYPED_TEST(DequeFixture, test37) {
    using deque_type = typename TestFixture::deque_type;
    // using iterator   = typename TestFixture::iterator;
    deque_type x(4,2);
    x.resize(8,4);
    ASSERT_EQ(x.size(),8);
    int a = x.back();
    ASSERT_EQ(a,4);
}

TYPED_TEST(DequeFixture, test38) {
    using deque_type = typename TestFixture::deque_type;
    // using iterator   = typename TestFixture::iterator;
    deque_type x(4,2);
    x.resize(3);
    int a = x.back();
    ASSERT_EQ(a,2);
    ASSERT_EQ(x.size(),3);
}
